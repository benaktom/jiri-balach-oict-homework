import process from "process";

import express, { Application } from "express";
import { Service } from "typedi";

import { AppConfig } from "./config/config";
import { MainRouter } from "./entryPoints/routes/main.router";

@Service()
export class App {
    private isRunning = false;
    private readonly app: Application;

    public constructor(
        private readonly appConfig: AppConfig,
        private readonly mainRouter: MainRouter,
    ) {
        this.app = this.build();
    }

    public async start() {
        if (this.isRunning) {
            return;
        }




        try {
            this.isRunning = true;
            const listener = this.app.listen(this.appConfig.getPort(), () => {
                console.log(
                    `Server running at http://localhost:${this.appConfig.getPort()}`,
                );
            });

            process.on("SIGINT", () => {
                listener.close(() => {
                    process.exit(0);
                });
            });
        } catch (err) {
            console.log("Application crashed!", err);
            process.exit(1);
        }
    }

    public getExpressApp(): Application {
        return this.app;
    }

    private build(): Application {
        const app = express();

        this.mainRouter.applyRoutes(app);

        return app;
    }
}
