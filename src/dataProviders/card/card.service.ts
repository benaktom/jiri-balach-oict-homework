import axios, { AxiosInstance } from "axios";
import { Service } from "typedi";

import {
    CardStateResponse,
    CardValidityResponse
} from "./card.response";
import { AppConfig } from "../../config/config";
import { HTTP_CODES } from "../../constants/httpCodes";

@Service()
export class CardService {
    private readonly apiConnection: AxiosInstance;
    constructor(config: AppConfig) {
        this.apiConnection = axios.create({
            baseURL: config.getCardApiUrl(),
            timeout: 10000,
        });
    }

    public async getCardStatus(cardNumber: number): Promise<CardStateResponse> {
        const cardStatusResponse =
            await this.apiConnection.get<CardStateResponse>(
                `/cards/${cardNumber}/state`,
            );

        if (cardStatusResponse.status === HTTP_CODES.OK) {
            return cardStatusResponse.data;
        }

        throw new Error(
            `Retrieve card status failed with ${cardStatusResponse.status}:${cardStatusResponse.statusText}!`,
        );
    }

    public async getCardValidity(
        cardNumber: number,
    ): Promise<CardValidityResponse> {
        const cardValidityResponse =
            await this.apiConnection.get<CardValidityResponse>(
                `/cards/${cardNumber}/validity`,
            );

        if (cardValidityResponse.status === HTTP_CODES.OK) {
            return cardValidityResponse.data;
        }

        throw new Error(
            `Retrieve card validity failed with ${cardValidityResponse.status}:${cardValidityResponse.statusText}!`,
        );
    }
}
