export interface CardStateResponse {
    state_id: number;
    state_description: string;
}

export interface CardValidityResponse {
    validity_start: string;
    validity_end: string;
}
