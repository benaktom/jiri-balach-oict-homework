import * as process from "process";

import { Service } from "typedi";

@Service()
export class AppConfig {
    private readonly port: number = ~~process.env.APP_PORT! || 3000;
    private readonly apiToken: string = process.env.API_AUTH_TOKEN || "x-dummy-token";
    private readonly cardApiUrl: string = process.env.LITACKA_API_URL || "";

    public getPort(): number {
        return this.port;
    }

    public getApiToken(): string {
        return this.apiToken;
    }

    public getCardApiUrl(): string {
        return this.cardApiUrl;
    }
}
