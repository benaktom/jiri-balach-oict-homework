import {
    NextFunction,
    Request,
    RequestHandler,
    Response
} from "express";
import { Service } from "typedi";

import { AppConfig } from "../../config/config";
import { HEADER } from "../../constants/header";
import { HTTP_CODES } from "../../constants/httpCodes";

@Service()
export class ApiKeyMiddleware {
    public constructor(private readonly appConfig: AppConfig) {}

    public handle: RequestHandler = (
        req: Request,
        res: Response,
        next: NextFunction,
    ) => {
        const token = req.header(HEADER.X_API_KEY);
        if (token && token === this.appConfig.getApiToken()) {
            next();
        } else {
            res.sendStatus(HTTP_CODES.UNAUTHORIZED);
        }
    };
}
