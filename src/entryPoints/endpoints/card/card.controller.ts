import {
    Request,
    RequestHandler,
    Response
} from "express";
import { Service } from "typedi";

import { CardNumberParameter } from "./request/request.parameters";
import { mapCardInfoResponse } from "./response/response.mapper";
import { HTTP_CODES } from "../../../constants/httpCodes";
import { CardService } from "../../../dataProviders/card/card.service";

@Service()
export class CardController {
    public constructor(private readonly cardService: CardService) {}

    public getCardInfoHandler: RequestHandler<CardNumberParameter> = async (
        req: Request<CardNumberParameter>,
        res: Response,
    ) => {
        try {
            const cardNumber = Number(req.params.cardNumber);

            if (isNaN(cardNumber)) {
                res.status(HTTP_CODES.BAD_REQUEST).json({
                    message: "Card number is not valid!",
                });
                return;
            }

            const cardStatus = await this.cardService.getCardStatus(cardNumber);
            const cardValidity =
                await this.cardService.getCardValidity(cardNumber);
            const cardInfoResponse = mapCardInfoResponse(
                cardStatus,
                cardValidity,
            );

            res.status(HTTP_CODES.OK).json(cardInfoResponse);
        } catch (e) {
            if (e instanceof Error) {
                res.status(HTTP_CODES.INTERNAL_SERVER_ERROR).json({
                    message: e.message,
                });
            }
            res.status(HTTP_CODES.INTERNAL_SERVER_ERROR);
        }
    };
}
