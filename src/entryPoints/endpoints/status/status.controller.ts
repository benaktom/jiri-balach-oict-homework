import {
    Request,
    RequestHandler,
    Response
} from "express";
import { Service } from "typedi";

import { HTTP_CODES } from "../../../constants/httpCodes";

@Service()
export class StatusController {
    public getStatusHandler: RequestHandler = (req: Request, res: Response) => {
        res.status(HTTP_CODES.OK).json({ status: "OK" });
    };
}
