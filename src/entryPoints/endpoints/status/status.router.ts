import { Router } from "express";
import { Service } from "typedi";

import { StatusController } from "./status.controller";

@Service()
export class StatusRouter {
    private readonly router: Router = Router();
    public constructor(private readonly statusController: StatusController) {
        this.router.get("/status", statusController.getStatusHandler);
    }

    public getRouter() {
        return this.router;
    }
}
