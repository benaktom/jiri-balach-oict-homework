export const formatDateISO8601ToEuropeDate = (dateStr: string): string => {
    const date = new Date(dateStr);

    if (isNaN(date.getTime())) {
        throw new Error("Wrong input date");
    }

    const day = date.getDate();
    const month = date.getMonth() + 1;
    const year = date.getFullYear();

    return `${day}.${month}.${year}`;
};
