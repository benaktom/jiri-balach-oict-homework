# OICT Homework

### Documentation
OpenAPI v3 documentation is available on **/swagger** endpoint when the application running or in the **_"./docs/open-api-v3.json"_** file


## Prerequisites

Recommended:
- Docker (https://www.docker.com/)
- Make

Or (installation instructions [see below](#install-and-run-without-docker)):
- node.js (https://nodejs.org)
- TypeScript (https://www.typescriptlang.org/)
- Yarn (https://yarnpkg.com/)

## Installation

Install all dependencies:
```bash
make install
```
OR
```bash
docker-compose up -d
docker-compose run --rm app sh -c "yarn install --network-timeout 1000000000"
```

## Development
Create the **.env** file with environment variables for application configuration
```bash
cp .env.example .env
```

### Run application
Application should run on port defined in **.env** (5000 if nothing changed)
```bash
make
```
OR
```bash
make run
```
OR withou make
```bash
docker-compose up -d
```

### Close application
#### To stop containers (application)
```bash
make stop
```
OR withou make
```bash
docker-compose stop
```
### Run tests
```bash
make test               # Run all tests
make test-unit          # Run unit tests
make test-integration   # Run integration tests
```

#

#### To stop and clear containers
```bash
make cleanup
```
OR withou make
```bash
docker-compose down
```



## Install and run without docker
### Install dependencies
```bash
yarn install
```

### Run application

**IMPORTANT**: Be sure to add all the env variables which are in **.env** or look at default values in "_**./src/config/config.js**_"
```bash
yarn run start:dev                # without env variables setup
APP_PORT=5000 yarn run start:dev  # with APP_PORT env set to 5000
```

### Run tests
```bash
yarn run test               # Run all tests
yarn run test:unit          # Run unit tests
yarn run test:integration   # Run integration tests
```
