FROM node:20.11.0-alpine as runtime

FROM runtime as dev
EXPOSE 5000
WORKDIR /app
ENV TZ=Europe/Prague
CMD [ "yarn", "start:dev" ]


FROM runtime as build
COPY src/ src/
COPY tsconfig.json tsconfig.json
COPY package.json package.json
COPY yarn.lock yarn.lock
RUN yarn install
RUN yarn run build


FROM runtime as prod
WORKDIR /app
COPY bin/ bin/
COPY docs/ docs/
COPY package.json package.json
COPY yarn.lock yarn.lock
COPY --from=build dist/ dist/
RUN yarn install --prod

USER 499:499

CMD ["/app/bin/homework-api"]
