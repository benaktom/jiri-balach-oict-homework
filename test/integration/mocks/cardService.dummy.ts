import {
    CardStateResponse,
    CardValidityResponse,
} from "../../../src/dataProviders/card/card.response";

export class CardServiceDummy {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    public async getCardStatus(cardNumber: number): Promise<CardStateResponse> {
        return {
            state_id: 100,
            state_description: "dummy description",
        };
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    public async getCardValidity(cardNumber: number): Promise<CardValidityResponse> {
        return {
            validity_start: "1970-01-01T00:00:00+01:00",
            validity_end: "2000-01-01T00:00:00+01:00",
        };
    }
}
