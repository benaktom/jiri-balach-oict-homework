import "reflect-metadata";

import { expect } from "chai";
import { describe } from "mocha";
import request from "supertest";
import { Container } from "typedi";

import { CardServiceDummy } from "./mocks/cardService.dummy";
import { App } from "../../src/app";
import { CardService } from "../../src/dataProviders/card/card.service";

describe("Card endpoint test", () => {
    before(() => {
        Container.set(CardService, new CardServiceDummy());
    });

    it("should return OK with card info", async () => {
        const app = Container.get(App);

        const response = await request(app.getExpressApp())
            .get("/card/12345")
            .set({ "x-api-key": "dev-token", Accept: "application/json" })
            .expect("Content-Type", /json/)
            .expect(200);

        expect(response.body).to.deep.equal( {
            valid_to: "1.1.2000",
            state: "dummy description",
        });
    });

    it("should return BAD REQUEST with not valid card number", async () => {
        const app = Container.get(App);

        const response = await request(app.getExpressApp())
            .get("/card/bad_number")
            .set({ "x-api-key": "dev-token", Accept: "application/json" })
            .expect("Content-Type", /json/)
            .expect(400);

        expect(response.body).to.deep.equal( { message: "Card number is not valid!" });
    });

    it("should return UNAUTHORIZED without header token", async () => {
        const app = Container.get(App);

        const response = await request(app.getExpressApp())
            .get("/card/bad_number")
            .expect(401);

        expect(response.body).to.deep.equal( {});
    });

    after(() => {
        Container.reset();
    });
});
