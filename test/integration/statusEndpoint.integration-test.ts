import "reflect-metadata";

import { expect } from "chai";
import { describe } from "mocha";
import request from "supertest";
import { Container } from "typedi";

import { App } from "../../src/app";

describe("Status endpoint test", () => {
    it("should return OK with status message", async () => {
        const app = Container.get(App);


        const response = await request(app.getExpressApp())
            .get("/status")
            .expect("Content-Type", /json/)
            .expect(200);

        expect(response.body).to.deep.equal( { status: "OK" });
    });
});
